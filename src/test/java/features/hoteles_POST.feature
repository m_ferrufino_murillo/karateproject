@API @hoteles
Feature: Tests relacionados a hoteles con endpoint POST

Background:
    * def hotels = "hotels"

@create
Scenario: Test para crear un Hotel
 Given url urlBase + hotels
  * def uuid = call read('classpath:js/utils/generateUUID.js')
  * def hotelName = "El Huesped " + uuid
  * def cityName =  call read('classpath:js/utils/getCity.js')
  * def rankingValue = function(x) { return Math.floor(Math.random() * 10) + 1; } 
   When def hotelRequest = 
   """ 
   {
     "name": "#(hotelName)",
     "description": "Hotel colonial cercano a la plaza principal",
     "city": "#(cityName)",
     "ranking": "#(rankingValue)"
   }
   """
   When request hotelRequest
   And method POST
   Then status 201
   