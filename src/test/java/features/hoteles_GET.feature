 @API_Hoteles
 Feature: Tests relacionados a hoteles con endpoint GET
 
  Background:
    * def hotels = "hotels"

  @smoke
  Scenario: Test lista de hoteles
    Given url urlBase + hotels
    When method GET
    Then status 200
    * def hotel = call read('classpath:js/utils/hotelEntity.js')
    * print hotel.getName()
    * def total = response.content.length
    * print response.totalElements
    #And assert response.totalElements == 21

  @integration
  Scenario: Test lista de hoteles
    Given url urlBase + hotels
    When method GET
    Then status 200
       And assert response.content.length > 2

  @integration  
  Scenario: Test que obtiene un hotel
    Given url urlBase + hotels + "/1"
    When method GET
    Then status 200
      And match response.name == 'El Huesped 37bb'
  