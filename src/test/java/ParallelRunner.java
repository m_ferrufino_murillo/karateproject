import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import com.intuit.karate.Results;
import com.intuit.karate.Runner;

import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.masterthought.cucumber.Configuration;
import net.masterthought.cucumber.ReportBuilder;

public class ParallelRunner {

   private final static Logger log = LoggerFactory.getLogger(ParallelRunner.class);    

    @Test
    public void testParallel() {
        Results results = Runner.parallel(getClass(), 5);

        String karateOutputPath = "target/surefire-reports";
        String tags = System.getProperty("ktags");
        if (tags == null || tags.isEmpty()) {
            tags = "@hoteles";
        }
        log.debug("Karate Tags: " + tags);
        List<String> arrayTags = Arrays.asList(tags.split(","));

        List<String> features = Arrays.asList("classpath:features");
        results = Runner.parallel(arrayTags, features , 1 , karateOutputPath);
        generateReport(karateOutputPath);
        assertTrue(results.getErrorMessages(), results.getFailCount() == 0);
    }

    public static void generateReport(String karateOutputPath) {
        Collection<File> jsonFiles = FileUtils.listFiles(new File(karateOutputPath), new String[] {"json"}, true);
        log.debug("files: " + jsonFiles);
        List<String> jsonPaths = new ArrayList<>(jsonFiles.size());
        jsonFiles.forEach(file -> jsonPaths.add(file.getAbsolutePath()));
        Configuration config = new Configuration(new File(karateOutputPath), "API Automation");
        ReportBuilder reportBuilder = new ReportBuilder(jsonPaths, config);
        reportBuilder.generateReports();
    }

}