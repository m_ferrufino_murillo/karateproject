function fn() {

    var env = karate.env; // get java system property 'karate.env'
    karate.log('karate.env system property was:', env);
    if (!env || env == null) {
        env = 'dev';
    }
    var config = { // base config JSON
        urlBase: 'http://localhost:8090/example/v1/'        
      };

    // don't waste time waiting for a connection or if servers don't respond within 5 seconds
    karate.configure('connectTimeout', 5000);
    karate.configure('readTimeout', 5000);
    var verbose = karate.properties['verbose'];
        if (verbose) {
            karate.log('running tests verbosely: ', verbose);
            karate.configure('printEnabled', verbose);
            karate.configure('logPrettyRequest', verbose);
        }

    karate.log('karate.env =', env);

    return config;
}